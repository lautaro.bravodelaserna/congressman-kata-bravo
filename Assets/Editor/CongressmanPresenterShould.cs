﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Presentation;
using UniRx;
using Views;

namespace Editor
{
    public class CongressmanPresenterShould
    {
        public string LAW = "aaaaaaaa";
        private CongressmanPresenter _presenter;
        private Mock<ICongressmanView> _congressmanView;
        private Mock<ICongressView> _view;
        private TestScheduler _customScheduler;
        private List<Congressman> congressman;

        [SetUp]
        public void Setup()
        {
            _congressmanView = new Mock<ICongressmanView>();
            congressman = new List<Congressman>{
                new Congressman(_congressmanView.Object, "Lauta"),
                new Congressman(_congressmanView.Object, "Facu"),
                new BoycottCongressman(_congressmanView.Object, "Andru"),
                new SubornCongressman(_congressmanView.Object, "Mauro")
            };
            _view = new Mock<ICongressView>();
            _presenter = new CongressmanPresenter(_view.Object, congressman);
            _customScheduler = new TestScheduler();
            
            Scheduler.DefaultSchedulers.Iteration = _customScheduler;
            Scheduler.DefaultSchedulers.TimeBasedOperations = _customScheduler;
            Scheduler.DefaultSchedulers.AsyncConversions = _customScheduler;
            Scheduler.DefaultSchedulers.ConstantTimeOperations = _customScheduler;
            Scheduler.DefaultSchedulers.TailRecursion = _customScheduler;
        }

        [Test]
        public void Start_Votation_When_Starts()
        {
            _presenter.StartVotation(LAW);
            _view.Verify(v => v.ShowStartVotation(LAW), Times.Once);
        }

        [Test]
        public void Show_Finish_Votation_When_Ends_Votation()
        {
            _presenter.StartVotation(LAW);
            _customScheduler.AdvanceBy(TimeSpan.FromMilliseconds(LAW.Length * 100 * 4).Ticks + 1);
            _view.Verify(v => v.FinishVotationWith(It.IsAny<string>()), Times.Once);
        }
    }
}
