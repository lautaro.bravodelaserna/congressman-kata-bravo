﻿using System;
using Laws;
using Moq;
using NUnit.Framework;
using UniRx;
using UnityEngine;
using Views;
using Votes;

namespace Editor
{
    public class AnxiousCongressmanShould
    {
        private const string TO_LONG_APPROVED_LAW = "TO LONG";
        private const VoteType DENIED_VOTE = VoteType.Denied;
        
        private AnxiousCongressman _congressMan;
        private TestScheduler _customScheduler;
        private Mock<ICongressmanView> _view;

        [SetUp]
        public void SetUp()
        {
            _view = new Mock<ICongressmanView>();
            _customScheduler = new TestScheduler();
            Scheduler.DefaultSchedulers.Iteration = _customScheduler;
            Scheduler.DefaultSchedulers.TimeBasedOperations = _customScheduler;
            Scheduler.DefaultSchedulers.AsyncConversions = _customScheduler;
            Scheduler.DefaultSchedulers.ConstantTimeOperations = _customScheduler;
            Scheduler.DefaultSchedulers.TailRecursion = _customScheduler;
        }
        
        [Test]
        public void Vote_For_Law()
        {
            VoteType currentVote = VoteType.None;
            GivenACongressMan();
            
            currentVote = WhenVoteWith(TO_LONG_APPROVED_LAW);
            ThenVoteIs(DENIED_VOTE, currentVote);
        }
        
        private void GivenACongressMan()
        {
            _congressMan = new AnxiousCongressman(_view.Object, "Ansioso");
        }
        
        private VoteType WhenVoteWith(string law)
        {
            VoteType currentVote = VoteType.None;

            _congressMan.GetVote(new Law(law))
                .Do(vote => currentVote = vote)
                .Subscribe();
            
            _customScheduler.AdvanceBy(TimeSpan.FromMilliseconds(law.Length * 100).Ticks + 1);
            return currentVote;
        }
        private void ThenVoteIs(VoteType type, VoteType currentVote)
        {
            Assert.AreEqual(type, currentVote);
        }
    }
}
