﻿using System;
using System.Collections.Generic;
using Laws;
using Moq;
using NUnit.Framework;
using UniRx;
using Views;

namespace Editor
{
    public class CongressCameraShould
    {
        private Mock<ICongressmanView> _congressmanView;
        private readonly Law APPROVED_LAW = new Law("APPROVE");
        private readonly Law DENIED_LAW = new Law("DENIEEEEEEEEEEEEEEEED");
        private const int THREE_CONGRESSMAN_AMOUNT = 3;
        private CongressCamera _camera;
        
        private TestScheduler _customScheduler;

        [SetUp]
        public void SetUp()
        {
            _congressmanView = new Mock<ICongressmanView>();
            _customScheduler = new TestScheduler();
            Scheduler.DefaultSchedulers.Iteration = _customScheduler;
            Scheduler.DefaultSchedulers.TimeBasedOperations = _customScheduler;
            Scheduler.DefaultSchedulers.AsyncConversions = _customScheduler;
            Scheduler.DefaultSchedulers.ConstantTimeOperations = _customScheduler;
            Scheduler.DefaultSchedulers.TailRecursion = _customScheduler;
        }
        
        [Test]
        public void Approved_Law_When_Most_Vote_Are_Positive_With_Delay()
        {
            var actualVote = LawStatement.None;
            var congressman = new List<Congressman>()
            {
                new Congressman(_congressmanView.Object, ""),
                new Congressman(_congressmanView.Object, ""),
                new Congressman(_congressmanView.Object, "")
            };

            GivenACongressCameraWith(congressman);
            actualVote = WhenVotesForLawWithDelay(APPROVED_LAW, THREE_CONGRESSMAN_AMOUNT);
            ThenLawStatementApprovedIsExpected(actualVote);
        }

        [Test]
        public void Not_Law_Voted_When_Not_All_Congressman_Voted_Yet()
        {
            var actualVote = LawStatement.None;
            var congressman = new List<Congressman>()
            {
                new Congressman(_congressmanView.Object, ""),
                new Congressman(_congressmanView.Object, ""),
                new Congressman(_congressmanView.Object, "")
            };

            GivenACongressCameraWith(congressman);
            actualVote = WhenVotesForLawWithoutDelay(APPROVED_LAW);
            ThenLawStatementNoneIsExpected(actualVote);
        }

        [Test]
        public void Throw_Boycott_Exception_When_A_Congressman_Is_Boycott()
        {
            var actualVote = LawStatement.None;
            var congressman = new List<Congressman>()
            {
                new Congressman(_congressmanView.Object, ""),
                new Congressman(_congressmanView.Object, ""),
                new BoycottCongressman(_congressmanView.Object, "")
            };

            GivenACongressCameraWith(congressman);
            actualVote = WhenVotesForLawWithDelay(APPROVED_LAW, THREE_CONGRESSMAN_AMOUNT);
            ThenLawStatementDeniedIsExpected(actualVote);
        }

        private void GivenACongressCameraWith(List<Congressman> congressman)
        {
            _camera = new CongressCamera(congressman);
        }

        private LawStatement WhenVotesForLawWithDelay(Law law, int congressmanAmount)
        {
            LawStatement actualVote = LawStatement.None;
            _camera.GetLawStatement(law)
                .Do(vote => actualVote = vote)
                .Subscribe();
            
            _customScheduler.AdvanceBy(TimeSpan.FromMilliseconds(law.GetLaw().Length * 100 * congressmanAmount).Ticks + 1);

            return actualVote;
        }

        private LawStatement WhenVotesForLawWithoutDelay(Law law)
        {
            LawStatement actualVote = LawStatement.None;
            _camera.GetLawStatement(law)
                .Do(vote => actualVote = vote)
                .Subscribe();
            return actualVote;
        }

        private void ThenLawStatementApprovedIsExpected(LawStatement actualVotes)
        {
            Assert.AreEqual(LawStatement.Approved, actualVotes);
        }

        private void ThenLawStatementDeniedIsExpected(LawStatement actualVotes)
        {
            Assert.AreEqual(LawStatement.Denied, actualVotes);
        }

        private void ThenLawStatementNoneIsExpected(LawStatement actualVote)
        {
            Assert.AreEqual(LawStatement.None, actualVote);
        }
    }
}