﻿using System;
using Laws;
using Moq;
using NUnit.Framework;
using UniRx;
using Views;
using Votes;

namespace Editor
{
    public class CongressSubornShould
    {
        private const string DENIED_LAW = "AAAAAAAAAAAAAAAAAAAAA";
        private const VoteType APPROVED_VOTE = VoteType.Approves;
        private const bool SUBORNED = true;
        
        private SubornCongressman _congressMan;
        private TestScheduler _customScheduler;
        private Mock<ICongressmanView> _view;

        [SetUp]
        public void SetUp()
        {
            _view = new Mock<ICongressmanView>();
            _customScheduler = new TestScheduler();
            Scheduler.DefaultSchedulers.Iteration = _customScheduler;
            Scheduler.DefaultSchedulers.TimeBasedOperations = _customScheduler;
            Scheduler.DefaultSchedulers.AsyncConversions = _customScheduler;
            Scheduler.DefaultSchedulers.ConstantTimeOperations = _customScheduler;
            Scheduler.DefaultSchedulers.TailRecursion = _customScheduler;
        }
        
        [Test]
        public void Vote_For_Law()
        {
            VoteType currentVote = VoteType.None;
            GivenACongressMan(SUBORNED);
            
            currentVote = WhenVoteWithSuborn(DENIED_LAW);
            ThenVoteIs(APPROVED_VOTE, currentVote);
        }
        
        private void GivenACongressMan(bool suborned)
        {
            _congressMan = new SubornCongressman(_view.Object, "", suborned);
        }
        
        private VoteType WhenVoteWithSuborn(string law)
        {
            VoteType currentVote = VoteType.None;

            _congressMan.GetVote(new Law(law))
                .Do(vote => currentVote = vote)
                .Subscribe();
            
            _customScheduler.AdvanceBy(TimeSpan.FromMilliseconds(law.Length * 100).Ticks + 1);
            return currentVote;
        }
        private void ThenVoteIs(VoteType type, VoteType currentVote)
        {
            Assert.AreEqual(type, currentVote);
        }
    }
}
