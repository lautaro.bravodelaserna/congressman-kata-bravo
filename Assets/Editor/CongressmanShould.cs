﻿using System;
using Laws;
using Moq;
using NUnit.Framework;
using UniRx;
using Views;
using Votes;

namespace Editor
{
    public class CongressManShould
    {
        private Congressman _congressMan;
        private Mock<ICongressmanView> _view;
        private TestScheduler _customScheduler;

        [SetUp]
        public void SetUp()
        {
            _view = new Mock<ICongressmanView>();
            _customScheduler = new TestScheduler();
            Scheduler.DefaultSchedulers.Iteration = _customScheduler;
            Scheduler.DefaultSchedulers.TimeBasedOperations = _customScheduler;
            Scheduler.DefaultSchedulers.AsyncConversions = _customScheduler;
            Scheduler.DefaultSchedulers.ConstantTimeOperations = _customScheduler;
            Scheduler.DefaultSchedulers.TailRecursion = _customScheduler;
        }
      
        [TestCase("Positive", VoteType.Approves)]
        [TestCase("NegativeVoteCongress", VoteType.Denied)]
        [TestCase("", VoteType.Abstain)]
        public void Vote_For_Law(string law, VoteType expectedVote)
        {
            VoteType currentVote = VoteType.None;
            GivenACongressMan();
            currentVote = WhenVoteWith(law);
            ThenVoteIs(expectedVote, currentVote);
        }

        [Test]
        public void Show_Name_When_Start_Vote()
        {
            VoteType currentVote = VoteType.None;
            GivenACongressMan();
            currentVote = WhenVoteWith("");
            _view.Verify(c => c.ShowName(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void Show_Type_Congressman_Start_Votation()
        {
            VoteType currentVote = VoteType.None;
            GivenACongressMan();
            currentVote = WhenVoteWith("");
            _view.Verify(c => c.ShowType(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void Show_Vote_When_Has_Vote()
        {
            VoteType currentVote = VoteType.None;
            GivenACongressMan();
            currentVote = WhenVoteWith("");
            _view.Verify(c => c.ShowVote(It.IsAny<string>()), Times.Once);
        }
        
        private void GivenACongressMan()
        {
            _congressMan = new Congressman(_view.Object, "");
        }
        private VoteType WhenVoteWith(string law)
        {
            VoteType currentVote = VoteType.None;
            _congressMan.GetVote(new Law(law))
                .Do(vote => currentVote = vote)
                .Subscribe();
            
            _customScheduler.AdvanceBy(TimeSpan.FromMilliseconds(law.Length * 100).Ticks + 1);
            return currentVote;
        }
        private void ThenVoteIs(VoteType type, VoteType currentVote)
        {
            Assert.AreEqual(type, currentVote);
        }
    }
}