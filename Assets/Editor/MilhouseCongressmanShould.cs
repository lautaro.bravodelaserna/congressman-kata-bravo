﻿using System;
using Laws;
using Moq;
using NUnit.Framework;
using UniRx;
using Views;
using Votes;

public class MilhouseCongressmanShould
{
    private const string APPROVED = "LAW";
    private const int TIME_GLASSES = 200;

    private const VoteType APPROVED_VOTE = VoteType.Approves;
        
    private MilhouseCongressman _congressMan;
    private TestScheduler _customScheduler;
    private Mock<ICongressmanView> _view;

    [SetUp]
    public void SetUp()
    {
        _view = new Mock<ICongressmanView>();
        _customScheduler = new TestScheduler();
        Scheduler.DefaultSchedulers.Iteration = _customScheduler;
        Scheduler.DefaultSchedulers.TimeBasedOperations = _customScheduler;
        Scheduler.DefaultSchedulers.AsyncConversions = _customScheduler;
        Scheduler.DefaultSchedulers.ConstantTimeOperations = _customScheduler;
        Scheduler.DefaultSchedulers.TailRecursion = _customScheduler;
    }
        
    [Test]
    public void Vote_For_Law()
    {
        VoteType currentVote = VoteType.None;
        GivenACongressMan();
            
        currentVote = WhenVoteWith(APPROVED);
        ThenVoteIs(APPROVED_VOTE, currentVote);
    }
        
    private void GivenACongressMan()
    {
        _congressMan = new MilhouseCongressman(_view.Object, "Milhouse");
    }
        
    private VoteType WhenVoteWith(string law)
    {
        VoteType currentVote = VoteType.None;

        _congressMan.GetVote(new Law(law))
            .Do(vote => currentVote = vote)
            .Subscribe();
            
        _customScheduler.AdvanceBy(TimeSpan.FromMilliseconds((law.Length * 100) + TIME_GLASSES).Ticks + 1);
        return currentVote;
    }
    private void ThenVoteIs(VoteType type, VoteType currentVote)
    {
        Assert.AreEqual(type, currentVote);
    }
}
