﻿namespace Laws
{
    public struct Law
    {
        private string _law;

        public Law(string law)
        {
            _law = law;
        }

        public string GetLaw()
        {
            return _law;
        }
    }
}