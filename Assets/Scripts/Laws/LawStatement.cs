﻿namespace Laws
{
    public enum LawStatement
    {
        Approved,
        Denied,
        None
    }
}