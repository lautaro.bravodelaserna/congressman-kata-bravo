﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Views;

public class CongressmanView : MonoBehaviour, ICongressmanView
{
    public void ShowName(string name)
    {
        Debug.Log("Nombre del congressman = " + name);
    }

    public void ShowType(string type)
    {
        Debug.Log("Tipo de Congressman = " + type);
    }

    public void ShowVote(string vote)
    {
        Debug.Log("Voto del Congressman = " + vote);
    }
}
