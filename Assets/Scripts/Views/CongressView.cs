﻿using System.Collections;
using System.Collections.Generic;
using Presentation;
using UnityEngine;
using Views;

public class CongressView : MonoBehaviour, ICongressView
{
    [SerializeField] private CongressmanView _view;
    
    public void Start()
    {
        var congressmanList = new List<Congressman>()
        {
            new Congressman(_view, "Lauta"),
            new Congressman(_view, "Facu"),
            new BoycottCongressman(_view, "Andru"),
            new SubornCongressman(_view, "Mauro")
        };
        var _presenter = new CongressmanPresenter(this, congressmanList);
        _presenter.StartVotation("Comela");
    }

    public void ShowStartVotation(string law)
    {
        Debug.Log("Empezando votacion con = " + law);
    }

    public void FinishVotationWith(string result)
    {
        Debug.Log("Resultado de la votacion = " + result);
    }
}
