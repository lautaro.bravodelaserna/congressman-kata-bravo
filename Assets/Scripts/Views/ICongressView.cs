﻿namespace Views
{
    public interface ICongressView
    {
        void ShowStartVotation(string law);
        void FinishVotationWith(string result);
    }

    public interface ICongressmanView
    {
        void ShowName(string isAny);
        void ShowType(string isAny);
        void ShowVote(string isAny);
    }
}
