﻿using System;
using System.Collections.Generic;
using Laws;
using UniRx;
using Votes;

public class CongressCamera
{
    private readonly List<Congressman> _congressman;
    private LawStatement _lawStatement;

    public CongressCamera(List<Congressman> congressman)
    {
        _congressman = congressman;
    }

    public IObservable<LawStatement> GetLawStatement(Law law)
    {
        var negativeVote = 0;
        var positiveVote = 0;

        return _congressman.ToObservable().SelectMany(c => c.GetVote(law))
            .Do(vote =>
            {
                if (vote == VoteType.Approves)
                    positiveVote++;
                else
                    negativeVote++;
            })
            .ContinueWith(Observable.Create<LawStatement>(o =>
            {
                _lawStatement = positiveVote > negativeVote ? LawStatement.Approved : LawStatement.Denied;
                o.OnNext(_lawStatement);
                return Disposable.Empty;
            }))
            .Catch((Func<BoycottException, IObservable<LawStatement>>) (_ => Observable.Return(LawStatement.Denied)));
    }
}