﻿using System;
using Laws;
using UniRx;
using Views;
using Votes;

public class MilhouseCongressman : Congressman
{
    private const int TIME_TO_PUT_GLASSES = 200;
    public MilhouseCongressman(ICongressmanView view, string name) : base(view, name)
    {
    }

    public override IObservable<VoteType> GetVote(Law law)
    {
        return Observable.Timer(TimeSpan.FromMilliseconds(TIME_TO_PUT_GLASSES)).SelectMany(x => base.GetVote(law));
    }
}