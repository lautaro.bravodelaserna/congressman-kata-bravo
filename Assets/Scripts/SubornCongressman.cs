﻿using Laws;
using Views;
using Votes;

public class SubornCongressman : Congressman
{
    private readonly ICongressmanView _view;
    private readonly string _name;
    private bool _isSuborned;

    public SubornCongressman(ICongressmanView _view, string name, bool isSuborned = false) : base(_view, name)
    {
        this._view = _view;
        _name = name;
        _isSuborned = isSuborned;
    }

    protected override VoteType GetCurrentVote(Law law)
    {
        _view.ShowName(_name);
        _view.ShowType(ToString());
        
        if (_isSuborned)
        {
            _view.ShowVote(VoteType.Approves.ToString());
            return VoteType.Approves;
        }

        return base.GetCurrentVote(law);
    }
}