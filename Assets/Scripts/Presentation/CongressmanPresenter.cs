﻿using System.Collections.Generic;
using Laws;
using UniRx;
using Views;

namespace Presentation
{
    public class CongressmanPresenter
    {
        private ICongressView _view;
        private CongressCamera _camera;
        private List<Congressman> _congressman;

        public CongressmanPresenter(ICongressView view, List<Congressman> congressman)
        {
            _congressman = congressman;
            _view = view;
            _camera = new CongressCamera(_congressman);
        }

        public void StartVotation(string law)
        {
            _view.ShowStartVotation(law);
            _camera.GetLawStatement(new Law(law))
                .Do(x => _view.FinishVotationWith(x.ToString()))
                .Subscribe();
        }
    }
}

