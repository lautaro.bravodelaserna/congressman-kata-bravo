﻿using System;
using System.Collections.Generic;
using Laws;
using UniRx;
using Views;
using Votes;

public class Congressman
{
    private readonly ICongressmanView _view;
    private readonly string _name;
    private List<IVote> _votes;
    public Congressman(ICongressmanView view, string name)
    {
        _view = view;
        _name = name;
        _votes = new List<IVote>
        {
            new AbstainVote(),
            new ApproveVote(),
            new DeniedVote(),
        };
    }
    public virtual IObservable<VoteType> GetVote(Law law)
    {
        return Observable.Timer(TimeSpan.FromMilliseconds(100 * law.GetLaw().Length)).ContinueWith(Observable.Create<VoteType>(
            o =>
            {
                var vote = GetCurrentVote(law);
                o.OnNext(vote);
                o.OnCompleted();
                _view.ShowName(_name);
                _view.ShowType(ToString());
                _view.ShowVote(vote.ToString());
                return Disposable.Empty;
            }));
    }

    protected virtual VoteType GetCurrentVote(Law law)
    {
        IVote currentVote = null;
        foreach (var vote in _votes)
        {
            if (vote.Applies(law))
                currentVote = vote;
        }
        return currentVote.GetVote();
    }
}