﻿using System;
using Laws;
using UniRx;
using Views;
using Votes;

public class AnxiousCongressman : Congressman
{
    private const int NUMBER_TO_LONG = 300;
    public AnxiousCongressman(ICongressmanView view, string name) : base(view, name)
    {
    }

    public override IObservable<VoteType> GetVote(Law law)
    {
        return base.GetVote(law)
            .Timeout(TimeSpan.FromMilliseconds(NUMBER_TO_LONG))
            .Catch((Func<TimeoutException, IObservable<VoteType>>) (_ => Observable.Return(VoteType.Denied)));
    }
}