﻿using System;
using Laws;
using UniRx;
using Views;
using Votes;

public class BoycottCongressman : Congressman
{
    private ICongressmanView _view;
    private readonly string _name;

    public BoycottCongressman(ICongressmanView view, string name) : base(view, name)
    {
        _view = view;
        _name = name;
    }

    public override IObservable<VoteType> GetVote(Law law)
    {
        _view.ShowName(_name);
        _view.ShowType(ToString());
        _view.ShowVote(VoteType.Denied.ToString());
        return Observable.Throw<VoteType>(new BoycottException());
    }
}