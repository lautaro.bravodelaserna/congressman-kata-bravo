﻿using Laws;

namespace Votes
{
    public class AbstainVote : IVote
    {
        public VoteType GetVote()
        {
            return VoteType.Abstain;
        }
        public bool Applies(Law law)
        {
            return law.GetLaw().Length <= 0;
        }
    }
}