﻿namespace Votes
{
    public enum VoteType
    {
        Approves,
        Denied,
        Abstain,
        None
    }
}