﻿using Laws;

namespace Votes
{
    public class DeniedVote : IVote
    {
        public VoteType GetVote()
        {
            return VoteType.Denied;
        }
        public bool Applies(Law law)
        {
            return law.GetLaw().Length > 9;
        }
    }
}