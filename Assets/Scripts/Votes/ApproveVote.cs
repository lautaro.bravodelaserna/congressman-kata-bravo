﻿using Laws;

namespace Votes
{
    public class ApproveVote : IVote
    {
        public VoteType GetVote()
        {
            return VoteType.Approves;
        }
        public bool Applies(Law law)
        {
            return law.GetLaw().Length <= 9 && law.GetLaw().Length > 0;
        }
    }
}