﻿using Laws;

namespace Votes
{
    public interface IVote
    {
        VoteType GetVote();
        bool Applies(Law law);
    }
}